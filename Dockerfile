FROM registry.gitlab.com/nexylan/docker/core:3.1.0 as core
FROM docker:20 as app
COPY --from=core / /
RUN setup
COPY rootfs /
ENTRYPOINT [ "/entrypoint.sh" ]

FROM app as test
WORKDIR /app
