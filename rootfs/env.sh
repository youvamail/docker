#!/usr/bin/env sh

echo "Docker environment setup:"

# Needed for some setup like Traefik configuration.
CI_ENVIRONMENT_HOST=$(echo "${CI_ENVIRONMENT_URL}" | sed 's~http[s]*://~~g')
export CI_ENVIRONMENT_HOST

# Default docker image name and tag to build.
# Automatic fallback to GitLab CI variable.
if [ -z "${IMAGE_NAME}" ]; then
	export IMAGE_NAME=${CI_REGISTRY_IMAGE:-nexylan/image}
fi
if [ -z "${IMAGE_TAG}" ]; then
	export IMAGE_TAG=${CI_COMMIT_REF_SLUG:-latest}
	if [ "${CI_COMMIT_REF_SLUG}" = "${CI_DEFAULT_BRANCH}" ]; then
		export IMAGE_TAG=latest
	elif [ -n "${CI_COMMIT_TAG}" ]; then
		export IMAGE_TAG=${CI_COMMIT_TAG#v}
	fi
fi
if [ -z "${IMAGE}" ]; then
	export IMAGE=${IMAGE_NAME}:${IMAGE_TAG}
fi

echo "CI_ENVIRONMENT_HOST: ${CI_ENVIRONMENT_HOST}"
echo "IMAGE_NAME: ${IMAGE_NAME}"
echo "IMAGE_TAG: ${IMAGE_TAG}"
echo "IMAGE: ${IMAGE}"
