#!/usr/bin/env sh
set -e

# shellcheck source=rootfs/env.sh
. /env.sh

login() {
	user=${1}
	password=${2}
	registry=${3:-docker.io}

	echo "login: ${user}@${registry}"
	docker login --username "${user}" --password "${password}" "${registry}"
}

# Docker hub login
if [ -n "${DOCKER_HUB_USER}" ] && [ -n "${DOCKER_HUB_PASSWORD}" ]; then
	login "${DOCKER_HUB_USER}" "${DOCKER_HUB_PASSWORD}"
fi

# GitLab CI registry login
if [ -n "${CI_REGISTRY}" ] && [ -n "${CI_REGISTRY_USER}" ] && [ -n "${CI_REGISTRY_PASSWORD}" ]; then
	login "${CI_REGISTRY_USER}" "${CI_REGISTRY_PASSWORD}" "${CI_REGISTRY}"
fi

/usr/local/bin/docker-entrypoint.sh "$@"
